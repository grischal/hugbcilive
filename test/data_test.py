#similar to the station_test.py file. If you need basic explanations, look there first
import unittest

#we use json here as a return format for the summarize method
#If you're unfamiliar with JSON, check the wikipedia article for a short intro
#Essentially, it's a very easy format to structure objects into strings
import json

#system under test is our WeatherData class
from app.station import WeatherData

class TestDataMethods(unittest.TestCase):
    def setUp(self):
        #Set up
        self.my_data = WeatherData()

    def test_collect(self):
        self.my_data.collect()
        
        #we know nothing about the values of the different sensors, just that they're between 0.0 and 1.0
        self.assertTrue(self.my_data.windSpeeds >= 0.0 and self.my_data.windSpeeds<1.0)
        self.assertTrue(self.my_data.airTemperatures >= 0.0 and self.my_data.airTemperatures<1.0)
        self.assertTrue(self.my_data.groundTemperatures >= 0.0 and self.my_data.groundTemperatures<1.0)
        self.assertTrue(self.my_data.windDirections >= 0.0 and self.my_data.windDirections<1.0)
        self.assertTrue(self.my_data.pressures >= 0.0 and self.my_data.pressures<1.0)
        self.assertTrue(self.my_data.rainfall >= 0.0 and self.my_data.rainfall<1.0)

    def test_summarize(self):
        #the summarize method requires us to first collect data
        #this is a design decision I made!
        self.my_data.collect()

        #run the summarize function and get a string back (we expect a JSON string)
        summary = self.my_data.summarize()
        #convert the json string into an object
        json_summary = json.loads(summary)
        #we expect that there is an airTemp field in the json object
        self.assertTrue("airTemp" in json_summary)
        #we expect the airTemp field to be between 0.0 and 1.0
        self.assertTrue(json_summary["airTemp"]<1.0 and json_summary["airTemp"]>=0)
        

if __name__ == '__main__':
    unittest.main()