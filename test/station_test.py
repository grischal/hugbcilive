#import unittest library - needed to run unit tests
import unittest

import json

#import the WeatherStation class - our test target ('system under test')
from app.station import WeatherStation

#You have to create a new class inheriting from unittest.TestCase
#All methods in this class will be run by the unittest runner!
class TestStationMethods(unittest.TestCase):
    #set up method - is run before each actual test case.
    def setUp(self):
        #all our tests need an instance of WeatherStation, so we just create one here
        self.my_station = WeatherStation("MyStation1")

    #This is a single test case - it runs the reportWeather function in our station
    #and makes sure the return value is an empty string
    def test_report_weather(self):
        #Run function
        weather = self.my_station.reportWeather()

        json_weather = json.loads(weather)

        #Assertion
        self.assertTrue(json_weather["airTemp"] >= 0.0 and json_weather["airTemp"] < 1.0)
        self.assertTrue(json_weather["groundTemp"] >= 0.0 and json_weather["groundTemp"] < 1.0)

    def test_report_status (self):
        #Run function
        status = self.my_station.reportStatus()

        #Assertion
        self.assertEqual("Status is all good.",status)

    #tear down method - is run after each test case
    def tearDown(self):
        print("Test done.")

#If this file is run with python, it will just execute all tests within this file
if __name__ == '__main__':
    unittest.main()