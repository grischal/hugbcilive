import random
import json

#A basic class that represents a weather station
#In this implementation, it just returns nonsense data
class WeatherStation:
    def __init__(self,identifier):
        self.identifier = identifier
        #we have a relation to WeatherData - so we want an instance of it
        self.data = WeatherData()

    #Whenever the report weather function is called, we ask the WeatherData instance
    #to collect the data, and then to summarise it - we return this summary
    def reportWeather(self):
        self.data.collect()
        return self.data.summarize()

    def reportStatus(self):
        return "Status is all good."

    def restart (self):
        return ""

    def shutdown (self):
        return ""

#A fake weather data class. Currently just randomly generates sensor values between 0.0 and 1.0
class WeatherData:
    def __init__(self):
        #I initialise all sensor values to -1.0.
        #Since this is out of the regular range (0.0 - 1.0),
        #I can actually check in my tests whether or not they have been changed!
        self.airTemperatures = -1.0
        self.groundTemperatures = -1.0
        self.windSpeeds = -1.0
        self.windDirections = -1.0
        self.pressures = -1.0
        self.rainfall = -1.0

    #Whenever collect is called, I just randomly generate values
    def collect(self):
        self.airTemperatures = random.random()
        self.groundTemperatures = random.random()
        self.windSpeeds = random.random()
        self.windDirections = random.random()
        self.pressures = random.random()
        self.rainfall = random.random()

    #Constructs a json string from all my sensor values and returns it.
    def summarize(self):
        #I've simplified it - it actually just returns two values
        return json.dumps({"airTemp":self.airTemperatures, "groundTemp":self.groundTemperatures, "windSpeed":self.windSpeeds})