#This example includes some more advanced concepts.
#To start with a basic websocket implementation, try to get the basic example from
#the original documentation to run, as we did in L7:
#https://websockets.readthedocs.io/en/stable/intro.html

#asyncio is used to ensure that we can do asynchronous operations (non-blocking)
import asyncio
import json
import websockets
import os

#We need our station code, to have actual "functionality"
#This represents our actual system.
#The code in this file represents the interface - the connection to the outside
import app.station

#This is our actual interface - the methods we expose to the outside
class ComponentInterface:
    def __init__ (self):
        self.station1 = app.station.WeatherStation("MyStation1")

    def reportWeather (self):
        #As you see here, our interface methods call functions in our actual system
        #implementation. Note that it is neither necessary to have exactly the same function names,
        #nor to have only one function call. I could do something like this instead:
        #self.station1.doA()
        #self.station1.doB()
        #return "success"
        return self.station1.reportWeather()

    def reportStatus (self):
        return self.station1.reportStatus()

#This class represents our technical connection to the outside.
#It allows processing of incoming text messages via WebSockets
#It is implemented as a Singleton pattern (which ensures there is only ever one instance of this class)
#Details on Singleton in the design lectures and here: https://en.wikipedia.org/wiki/Singleton_pattern
class ComponentPort:
    #This is our (private) instance
    __instance = None

    #This is a class method - it can be called without an instance existing
    @staticmethod 
    def get_instance():
        """ Static access method. """
        if ComponentPort.__instance == None:
            #in this particular case, the method makes sure that an instance is created if none exists
            ComponentPort()      
        
        #afterwards (or if an instance did already exist), we return the instance
        return ComponentPort.__instance

    #The constructor throws an exception if we already have an instance - we don't want to allow any other instances      
    #Note: This is actually not a nice implementation in Python. In other languages, you can actually force that only one instance is created.
    #In Python, this is more of a convention...
    def __init__(self):
        if ComponentPort.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            #If there is no instance, we create one
            ComponentPort.__instance = self
            self.iface = ComponentInterface()

    #Up to here, all the code was just Singleton stuff.
    #This is the real WebSockets meat
    async def __msg_handler(self, websocket, path):
        #We wait until we receive a message (from a client/other system)
        msg = await websocket.recv()
        #We assume that the received msg is JSON and convert it to an object
        #Note that in a real system you want to have some exception handling here...
        data = json.loads(msg)

        #We have decided (design decision) that we expect a JSON string that has an 'op' field.
        #This op field contains the interface method we want to call
        if "op" in data:
            try:
                print("trying to call %s"%(data["op"]))
                #This might be cryptic: we try to call the method in our ComponentInterface instance
                #that has the same name as data["op"].
                #You could also have multiple if/else statements here that check for the values of "op"
                op_return = getattr(self.iface, data["op"])()
                #we take whatever our function call returns and convert it to json
                return_value = json.dumps({"msg":op_return})

                ####
                #### In this example, all our interface methods have no parameters.
                #### In your case, you will have parameters (e.g., a username if you want to create a new user)
                #### You will have to put these paramteres also into the JSON string
                #### E.g., for the "create user" scenario, you probably will have something checking whether data["username"] exists, then put that into the method as a param
                ####

            except AttributeError:
                #if we cannot call the method (Line 80 above), we return an error (in form of a JSON string message)
                return_value = json.dumps({"msg":"Method %s not implemented"%(data["op"])})

        else:
            #if there is no "op" field in the JSON string, we return an error (again as a JSON string message)
            return_value = json.dumps({"msg":"Invalid data format"})

        #We return our return message (depending on the outcome a different JSON string)
        await websocket.send(return_value)

    #This logic starts the websocket server and listens until we kill the application
    #In the basic example, this code is just below the actual method
    #Here, I placed it into a function so that I can just call it below in the main part (lines 114-117)
    def start(self, port):
        #Note here that __msg_handler is the name of the function that should be called whenever a message arrives
        #That's the function defined above (Line 65)
        start_server = websockets.serve(self.__msg_handler, "0.0.0.0", port)
        
        #Run until forever
        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8080))

    #Here we get out ComponentPort Singleton (our single instance) and start the WebSockets server
    compPort = ComponentPort.get_instance()
    compPort.start(port)